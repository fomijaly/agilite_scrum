## Conception - agilité et scrum

1. Pourquoi le cycle en V a-t-il été créé ?
> Le cycle en V a été créé pour améliorer la méthode waterfall. En effet, il permet lors de la validation de se référer directement à la documentation nécessaire en évitant de faire des aler-retours dans le cycle de vie du projet.

2. Quels sont les inconvénients du cycle en V ?
> La difficulté de revenir sur une étape précédente dans le cycle en V, sans débuter de nouveau cycle, rend la gestion du projet plus compliqué.
De plus, le cycle peut être trop long et dans ce cas le rendu final ne sera plus adapté à la demande qui aura potentiellement évolué. 

3. Pour quelles raisons le manifeste agile a-t-il été créé ?
> Le manifest agile a été créé pour répondre plus précisément aux besoins de l'entreprise tout au long du cycle de vie du projet, pour obtenir un résultat final qui correspond malgré les modifications en cours de route.

4. Quelles sont les quatre valeurs de l'agilité ?
> * Valoriser les individus et leurs interactions plus que les processus et les outils. 
> * Mettre en place des outils opérationnels, plutôt que de la doc exhaustive.
> * Prioriser la collaboration avec les clients plutôt que la négociation contractuelle.
> * L'adaptation au changement plutôt que le suivi d'un plan - Être flexible

5. C'est quoi un Product Backlog ?
> C'est une liste de tâches et de fonctionnalités (ou des users stories) à effectuer qui sont hiérarchisées et priorisées et qui permettront d'atteindre l'objectif final du projet.  Il comprend les caractéristiques, les fonctions, les exigences, les améliorations et les corrections, ce qui autorise toute modification du produit dans les versions ultérieures.

6. C'est quoi un Sprint ?
> Un sprint c'est une courte période déterminée, pendant laquelle des tâches et des objectifs **atteignables** doivent être réalisés.

7. Quel est le rôle d'un Product Owner ?
> Le Product owner représente les parties prenantes et est la voix du client. Il retranscrira à l'équipe technique les besoins commerciaux . 

8. Quel est le rôle d'un Scrum Master ?
> Le Scrum Master assure la bonne compréhension et l'exécution de la méthode SCRUM tout au long du développement du projet.

9. C'est quoi un Kanban ?
> C'est une méthode qui permet de visualiser très rapidement le flux de travail, grâce à des repères visuels (couleurs par exemple), afin de limiter les travaux en cours, et d'obtenir facilement des feedbacks.

10. En quoi consiste le planning poker ?
> Le planning Poker est basé, lors de sa réalisation, sur le ressenti et les différents points de vue des membres de l'équipe / experts techniques afin d'estimer la difficulté nécessaire à la réalisation de chaque fonctionnalité.